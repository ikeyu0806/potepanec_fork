require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    context "solidusのfactoryが渡された場合" do
      let(:product) { create(:product) }

      before { get :show, { :params => { :id => product.id } } }

      it "renders the :show template" do
        expect(response).to render_template(:show)
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "assigns @product" do
        expect(assigns(:product)).to eq product
      end
    end

    context "複数カテゴリの商品が渡された場合" do
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:brands_taxonomy)   { create(:taxonomy, name: 'Brands') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:mugs_taxon)        { create(:taxon, name: 'Mugs', taxonomy: category_taxonomy) }
      let(:apache_taxon)      { create(:taxon, name: 'Apache', taxonomy: brands_taxonomy) }
      let(:rails_taxon)       { create(:taxon, name: 'Rails', taxonomy: brands_taxonomy) }

      let(:bags_products) do
        create_list(:product, 5) do |product|
          product.taxons << bags_taxon
          product.taxons << apache_taxon
        end
      end

      let(:mugs_products) do
        create_list(:product, 5) do |product|
          product.taxons << mugs_taxon
          product.taxons << rails_taxon
        end
      end

      it "関連商品に同じカテゴリーの製品が含まれること" do
        bags_product = bags_products.first
        related_products = (bags_products - [bags_product])
        get :show, params: { id: bags_product.id }
        expect(assigns(:related_products)).to match_array related_products
      end

      it '関連商品に他のカテゴリーの製品が含まれないこと' do
        mugs_product = mugs_products.first
        get :show, params: { id: mugs_product.id }
        expect(assigns(:related_products)).not_to include(bags_products)
      end
    end

    it "最大表示数が８に設定されていること" do
      expect(Potepan::ProductsController::MAX_RELATED_PRODUCTS).to eq 8
    end

    context "渡された商品数が最大表示数より多い場合" do
      let(:max_display) { 8 }
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:product) do
        create(:product) do |product|
          product.taxons << bags_taxon
        end
      end

      let!(:products_in_bag_category) do
        create_list(:product, 10) do |product|
          product.taxons << bags_taxon
        end
      end

      it "表示される商品数が最大表示数と等しいこと" do
        get :show, params: { id: product.id }
        expect(assigns(:related_products).count).to eq max_display
      end
    end

    context "渡された商品数が最大表示数より少ない場合" do
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:product) do
        create(:product) do |product|
          product.taxons << bags_taxon
        end
      end

      let!(:products_in_bag_category) do
        create_list(:product, related_product_counts) do |product|
          product.taxons << bags_taxon
        end
      end

      let(:related_product_counts) { 5 }

      it "表示される関連商品の数が同じカテゴリーの商品数と一致すること" do
        get :show, params: { id: product.id }
        expect(assigns(:related_products).count).to eq related_product_counts
      end
    end
  end

  describe "GET #index" do
    context "solidusのfactoryが渡された場合" do
      let(:variant) { create(:variant) }

      before { get :index, { :params => { :id => variant.id } } }

      it "renders the :show template" do
        expect(response).to render_template(:index)
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "assigns @variant" do
        expect(assigns(:variant)).to eq variant
      end
    end

    context "複数カテゴリの商品が渡された場合" do
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:brands_taxonomy)   { create(:taxonomy, name: 'Brands') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:mugs_taxon)        { create(:taxon, name: 'Mugs', taxonomy: category_taxonomy) }
      let(:apache_taxon)      { create(:taxon, name: 'Apache', taxonomy: brands_taxonomy) }
      let(:rails_taxon)       { create(:taxon, name: 'Rails', taxonomy: brands_taxonomy) }

      let(:bags_products) do
        create_list(:product, 5) do |product|
          product.taxons << bags_taxon
          product.taxons << apache_taxon
        end
      end

      let(:mugs_products) do
        create_list(:product, 5) do |product|
          product.taxons << mugs_taxon
          product.taxons << rails_taxon
        end
      end

      let(:bags_variant) { create(:variant, product: bags_products.first) }
      let(:mugs_variant) { create(:variant, product: mugs_products.first) }

      it "関連商品に同じカテゴリーの製品が含まれること" do
        bags_product = bags_products.first
        get :index, params: { id: bags_variant.id }
        expect(assigns(:related_products)).to include bags_product
      end

      it '関連商品に他のカテゴリーの製品が含まれないこと' do
        bags_product = bags_products.first
        get :index, params: { id: mugs_variant.id }
        expect(assigns(:related_products)).not_to include bags_product
      end
    end

    context "渡された商品数が最大表示数より多い場合" do
      let(:max_display) { 8 }
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:product) do
        create(:product) do |product|
          product.taxons << bags_taxon
        end
      end

      let!(:products_in_bag_category) do
        create_list(:product, 10) do |product|
          product.taxons << bags_taxon
        end
      end

      let(:bags_variant) { create(:variant, product: products_in_bag_category.first) }

      it "表示される商品数が最大表示数と等しいこと" do
        get :index, params: { id: bags_variant.id }
        expect(assigns(:related_products).count).to eq max_display
      end
    end

    context "渡された商品数が最大表示数より少ない場合" do
      let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: category_taxonomy) }
      let(:product) do
        create(:product) do |product|
          product.taxons << bags_taxon
        end
      end

      let!(:products_in_bag_category) do
        create_list(:product, related_product_counts) do |product|
          product.taxons << bags_taxon
        end
      end

      let(:related_product_counts) { 5 }

      let(:bags_variant) { create(:variant, product: products_in_bag_category.first) }

      it "表示される関連商品の数が同じカテゴリーの商品数と一致すること" do
        get :show, params: { id: product.id }
        expect(assigns(:related_products).count).to eq related_product_counts
      end
    end
  end
end
