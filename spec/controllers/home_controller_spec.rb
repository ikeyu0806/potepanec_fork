require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    before { get :index }

    let(:max_display) { 8 }
    let(:new_date) { "2018-01-%.2d" }
    let(:old_date) { "2017-01-%.2d" }
    let(:future_date) { "2100-01-01" }

    let!(:old_products) do
      1.upto(3) do |n|
        create(:product, name: "old_product", available_on: old_date % n)
      end
    end

    let!(:new_products) do
      1.upto(max_display - 1) do |n|
        create(:product, name: "new_product", available_on: new_date % n)
      end
    end

    let!(:latest_product) do
      create(:product, name: "new_product", available_on: new_date % 31)
    end

    let(:future_product) { create(:product, name: "future_product", available_on: :future_date) }

    it "renders the :index template" do
      expect(response).to render_template(:index)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "最大表示数が８に設定されていること" do
      expect(Potepan::HomeController::MAX_NEW_PRODUCTS).to eq max_display
    end

    it "新着商品の表示数が８であること" do
      expect(assigns(:new_products).count).to eq 8
    end

    it "新しい商品が表示されていること" do
      0.upto(max_display - 1) do |n|
        expect(assigns(:new_products)[n].name).to eq "new_product"
      end
    end

    it "古い商品が表示されていないこと" do
      expect(assigns(:new_products)).not_to include old_products
    end

    it "最新日付の商品が最初に表示されていること" do
      expect(assigns(:new_products).first).to eq latest_product
    end

    it "未来日付の商品が表示されないこと" do
      expect(assigns(:new_products)).not_to include future_product
    end
  end
end
