require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:bags_taxon) { create(:taxon, name: 'Bags') }
    let(:mugs_taxon) { create(:taxon, name: 'Mugs') }
    let!(:category_taxonomy) { create(:taxonomy, taxon_ids: bags_taxon.id) }
    let(:color_option_type) { create(:option_type, presentation: 'Color', id: '2') }
    let(:red_option_value) { create(:option_value, name: 'Red', option_type: color_option_type) }
    let!(:bags_products) do
      create_list(:product, 10) do |product|
        product.taxons << bags_taxon
      end
    end
    let(:mugs_product) { create(:product, name: 'Mugs', taxon_ids: mugs_taxon.id) }

    before { get :show, { :params => { :id => bags_taxon.id } } }

    it "renders the :show template" do
      expect(response).to render_template(:show)
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it "要求したtaxonが割り当てられているか" do
      expect(assigns(:taxon)).to eq bags_taxon
    end

    it "taxonの属するtaxonomyが割り当てられているか" do
      expect(assigns(:taxonomies)).to include category_taxonomy
    end

    it "カテゴリ通りに商品が割り当てられているか" do
      expect(assigns(:products)).to eq bags_products
    end

    it "違うカテゴリの商品が割り当てられていないか" do
      expect(assigns(:products)).not_to eq mugs_product
    end

    it "サイドバーにカテゴリが表示されること" do
      expect(assigns(:taxonomies).to_a).to include category_taxonomy
    end

    it "サイドバーに色情報が表示されること" do
      expect(assigns(:colors_in_sidebar)).to include red_option_value
    end
  end

  describe "GET #index" do
    let(:bags_taxon) { create(:taxon, name: 'Bags') }
    let!(:category_taxonomy) { create(:taxonomy, taxon_ids: bags_taxon.id) }
    let(:color_option_type) { create(:option_type, presentation: 'Color', id: '2') }
    let(:red_option_value) { create(:option_value, name: 'Red', option_type: color_option_type) }
    let(:blue_option_value) { create(:option_value, name: 'Blue', option_type: color_option_type) }

    let!(:red_variant) do
      create_list(:variant, 5) do |variant|
        variant.option_values << red_option_value
      end
    end

    let!(:blue_variant) do
      create_list(:variant, 5) do |variant|
        variant.option_values << blue_option_value
      end
    end

    before { get :index, { :params => { :color => red_option_value.name } } }

    it "renders the :index template" do
      expect(response).to render_template(:index)
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it "要求したvariantが割り当てられているか" do
      expect(assigns(:variants).to_a).to match_array red_variant
    end

    it "異なる色のvariantが割り当てられていないか" do
      expect(assigns(:variants).to_a).not_to match_array blue_variant
    end

    it "サイドバーにカテゴリが表示されること" do
      expect(assigns(:taxonomies).to_a).to include category_taxonomy
    end

    it "サイドバーに色情報が表示されること" do
      expect(assigns(:colors_in_sidebar)).to include red_option_value
    end
  end
end
