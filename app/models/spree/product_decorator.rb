Spree::Product.class_eval do
  scope :eager_load_products, -> { includes(master: [:default_price, :images]) }
  scope :limit_products_by_today, -> { where('available_on < ?', Time.now) }
  scope :load_by_taxon, -> taxon { where(spree_taxons: { id: taxon.id }) }
  scope :load_by_taxons, -> taxon { where(spree_taxons: { id: taxon.ids }) }
  scope :load_descendants, -> taxon { where(spree_taxons: { id: [taxon.descendants] }) }
  scope :load_by_product, -> product { where(spree_taxons: { id: product.taxon_ids }) }
  scope :exclude_self, -> product { where.not(spree_products: { id: product.id }) }

  class << self
    def load_products(taxon)
      if taxon.leaf?
        eager_load_products.load_by_taxon(taxon)
      else
        eager_load_products.load_descendants(taxon)
      end
    end

    def limit_products(product, max_display)
      eager_load_products.exclude_self(product).limit(max_display).uniq
    end

    def load_related_products(product, max_display)
      joins(:taxons).load_by_product(product).limit_products(product, max_display)
    end

    def load_new_products(max_display)
      eager_load_products.eager_load_products.limit_products_by_today.
        limit(max_display).order(available_on: :desc)
    end

    def limit_load_by_taxon(taxon, max_display)
      load_by_taxons(taxon).limit(max_display)
    end
  end
end
