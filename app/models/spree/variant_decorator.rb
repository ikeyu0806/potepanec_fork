Spree::Variant.class_eval do
  scope :load_by_option_values, -> id { where(spree_option_values: { id: id }) }
  scope :load_by_option_values_name, -> name { where(spree_option_values: { name: name }) }
end
