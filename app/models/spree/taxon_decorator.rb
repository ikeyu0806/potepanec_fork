Spree::Taxon.class_eval do
  scope :load_by_variant, -> variant { where(spree_products: { id: variant.product_id }) }
end
