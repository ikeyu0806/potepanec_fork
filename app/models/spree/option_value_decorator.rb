Spree::OptionValue.class_eval do
  scope :load_by_option_type_id, -> option_type_id { where(option_type_id: option_type_id) }
end
