class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS = 8

  def index
    @variant = Spree::Variant.find(params[:id])

    taxon = Spree::Taxon.joins(:products).load_by_variant(@variant)

    @related_products = Spree::Product.joins(:taxons).
      limit_load_by_taxon(taxon, MAX_RELATED_PRODUCTS)
  end

  def show
    @product = Spree::Product.find(params[:id])

    @related_products = Spree::Product.joins(:taxons).
      load_related_products(@product, MAX_RELATED_PRODUCTS)
  end
end
