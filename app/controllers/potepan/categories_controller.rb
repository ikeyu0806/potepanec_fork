class Potepan::CategoriesController < ApplicationController
  include Sidebar
  before_action :set_sidebar_variables

  def index
    @variants = Spree::Variant.joins(:option_values).
      load_by_option_values_name(params[:color]).to_a.uniq { |color| color.name }
  end

  def show
    @taxon = Spree::Taxon.find(params[:id])

    @products = Spree::Product.joins(:taxons).load_products(@taxon)
  end

  def set_sidebar_variables
    @taxonomies = taxonomies

    @colors_in_sidebar = colors
  end
end
