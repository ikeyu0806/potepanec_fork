class Potepan::HomeController < ApplicationController
  MAX_NEW_PRODUCTS = 8

  def index
    @new_products = Spree::Product.load_new_products(MAX_NEW_PRODUCTS)
  end
end
