module Sidebar
  extend ActiveSupport::Concern
  COLOR_ID = 2

  def taxonomies
    Spree::Taxonomy.includes(:taxons)
  end

  def colors
    Spree::OptionValue.load_by_option_type_id(COLOR_ID)
  end
end
